#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QTableView>
#include <QStandardItemModel>

#include <QFileDialog>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->statusBar->showMessage(tr("Ready"));
    ui->outputGroupBox->hide();
    connect(ui->checkButton, SIGNAL(pressed()), ui->outputGroupBox, SLOT(show()));
    connect(ui->filterComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(changeExportButtonText(int)));

    connect(ui->checkButton, SIGNAL(pressed()), this, SLOT(textProcessor()));
    ui->filterComboBox->setCurrentIndex(0);

    connect(ui->exportButton, SIGNAL(pressed()), this, SLOT(saveAsText()));

    connect(ui->actionLoad_from_file, SIGNAL(triggered()), this, SLOT(setFilePath()));
    connect(ui->actionAbout_Qt, SIGNAL(triggered()), this, SLOT(showAboutQt()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showAboutQt()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::setFilePath()
{
    filePath = QFileDialog::getOpenFileName(this, "Open plain text file", "", tr("Plain text file (*.txt *.sql *.csv)"));
    this->textProcessor();
}

void MainWindow::changeExportButtonText(int checkingState)
{
    qDebug() << "current index is" << checkingState;
    switch (checkingState)
    {
        case 0: {
            ui->exportButton->setText("Export all into text");
            break;
        }
        case 1: {
            ui->exportButton->setText("Export VALID as a text");
            break;
        }
        case 2: {
            ui->exportButton->setText("Export INVALID as a text");
            break;
        }
    }
}

void MainWindow::textProcessor()
{
    // clearing QString rows vars if cycle was already running
    allRows.clear();
    invalidRows.clear();
    validRows.clear();
    plainText.clear();
    // main processing thread
    qDebug() << "text processor";
    QTextCursor cursor = ui->inputPlainTextEdit->textCursor();
    cursor.setPosition(0);
    //
    if(!this->filePath.isEmpty())
    {
        QFile externalFile(this->filePath);
        if(!externalFile.open(QIODevice::ReadOnly))
        {
            QMessageBox::information(this, "Error while opening the file", externalFile.errorString());
        }
        else
        {
            QTextStream in(&externalFile);
            ui->statusBar->showMessage("Reading content from external file...");
            plainText.append("\n,");
            plainText = in.readAll();            // this is fucking slow operation
            externalFile.close();
            ui->statusBar->showMessage("Counting rows...");
            ui->outputGroupBox->show();
        }
        this->filePath.clear();
    }
    else
    {
        plainText = ui->inputPlainTextEdit->toPlainText();
    }

    QStringList stringList = plainText.split("\n");                   // текст в массив строк

    //////////////
    QStandardItemModel *model = new QStandardItemModel(stringList.count(), 0, ui->listView); // create new item model

    //if(stringList.count() > 10000) ui->statusBar->showMessage();
    stringList.count() > 10000 ? ui->statusBar->showMessage("There is "+ QString::number(stringList.count()) +" rows, please wait while output is rendering...") : ui->statusBar->showMessage("Calculating, please wait...");

    ui->listView->setModel(model);

    QVector<QString> stringVector;                                    // создаем вектор для вывода данных
    int stringIterator = 0;
    QCoreApplication::processEvents();
    foreach (QString str, stringList) {                               // итерация для каждой строки (можно вынести в отд. ф-цию)
        // FIXME : refactor for manual memory control
        // FIXME : prevert interface hung by using multithread model
        QStringList separateStringByBlock;
        if (str.contains(","))
            separateStringByBlock = str.split(",");
        else
        if (str.contains("|"))
            separateStringByBlock = str.split("|");

        QStandardItem *mItem = new QStandardItem(str);                    // !! ALLOCATED item
        QModelIndex index = model->index(stringIterator, 0);
        model->setItem(stringIterator, mItem);
        //
        foreach (QString block, separateStringByBlock) { // iterate STRINGS
            qDebug() << block.contains(QRegularExpression("\\d{12}+"));
            if(block.contains(QRegularExpression("\\d{12}+")))
            {
                block.replace("\'","");
                if(this->luhnChecker(block))
                {
                    model->setData(index, QColor(0,200,0,200), Qt::BackgroundRole); // set color
                    validRows.append(index.data().toString()).append("\n");
                    qDebug() << "CC card is OKAY";
                    //ui->statusBar->showMessage("CC number is OK");
                }
                else
                {
                    model->setData(index, QColor(Qt::red), Qt::BackgroundRole); // set color
                    invalidRows.append(index.data().toString()).append("\n");
                    qDebug() << "CC card is INVALID";
                    //ui->statusBar->showMessage("CC number is INVALID");
                }

                break;
            }
        }
        ++stringIterator;
    }
    // FIXME: add count of valid and invalid rows separately
    QCoreApplication::processEvents();
    qDebug() << stringVector.count();
    ui->statusBar->showMessage("Calculating is done. Number of rows: " + QString::number(stringList.count()));
    if(ui->filterComboBox->currentIndex() == 0)
    {
        allRows = plainText;
    }
    plainText.clear();
}


// LUHN CHECKER IS NEED TO MOVE INTO BACKGROUD THREAD
bool MainWindow::luhnChecker(QString inputString)
{
    qDebug() << "LUHN_CHECKER_INIT...";
    QString multipledString;

    std::reverse(inputString.begin(), inputString.end()); // reversing | QString is iterable
    qDebug() << "REVERSED: " << inputString;
    qDebug() << "DIGITS:" << inputString.count();
    for(int i = 0; i < inputString.count(); i++)
    {
        if(i % 2 != 0)
            multipledString.append(QString::number(QString(inputString.at(i)).toInt() * 2));
        else
            multipledString.append(inputString.at(i));
    }

    qDebug() << "ASSIGNED: \"" << multipledString << "\"";
    int checksum = 0;
    foreach (QString charDigit, multipledString)
    {
        checksum += charDigit.toInt();
    }

    qDebug() << "CHECKSUM: \"" << checksum << "\"";
    if(checksum != 0 && checksum % 10 == 0)
        return true;
    else
        return false;
}

void MainWindow::saveAsText()
{
    QString filenamePWD = QFileDialog::getSaveFileName(this, "Save file as...", "", tr("Plain text file (*.txt)"));
    QFile file(filenamePWD);
    if(file.open(QIODevice::ReadWrite))
    {
        QTextStream stream(&file);
        switch(ui->filterComboBox->currentIndex())
        {
        // FIXME: there is may be bugs with clearing data in clear()
            case 0: { stream << allRows; allRows.clear(); break; }
            case 1: { stream << validRows; validRows.clear(); break; }
            case 2: { stream << invalidRows; invalidRows.clear(); break; }
        }
    file.close();
    }
    else
    {
        QMessageBox::critical(this, "Error while saving file", "Error while saving file. Please change file saving place or report to developer");
    }
}
