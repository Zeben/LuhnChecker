#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QDebug>

#include <QStringList>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    bool luhnChecker(QString inputString);
    QString invalidRows, validRows, allRows, filePath, plainText;


private slots:
    void changeExportButtonText(int checkingState);
    void textProcessor();
    void saveAsText();
    void setFilePath();
    void showAboutQt();
};

#endif // MAINWINDOW_H
